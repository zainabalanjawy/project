// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'project-9c378',
    appId: '1:794177902669:web:b925244872ef85b3572404',
    databaseURL: 'https://project-9c378-default-rtdb.firebaseio.com',
    storageBucket: 'project-9c378.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyAjwxyHV2RxZAlqfpV2VEonMk7GzxFj6u8',
    authDomain: 'project-9c378.firebaseapp.com',
    messagingSenderId: '794177902669',
    measurementId: 'G-7KBSWF71JR',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
