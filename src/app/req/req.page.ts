import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { DataService } from '../data.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';


@Component({
  selector: 'app-req',
  templateUrl: './req.page.html',
  styleUrls: ['./req.page.scss'],
})
export class ReqPage implements OnInit {

  uid;
  users$;
  schedual$;

  constructor(public afs:AngularFirestore,public Dataservs:DataService, public router:Router)
  { 
    this.uid = localStorage.getItem("uid")
    
    this.users$ = this.Dataservs.getuser();
    this.schedual$ = this.Dataservs.getschedule();


  }

  ngOnInit() 
  {

  }


  pages =
  [
    {
      title: 'Acount',
      url: '/account',
      icon: 'person'
    },
    {
      title: 'sign-out',
      url: '/account',
      icon: 'log-out'
    }
  ];

  async approve(x)
  {
    //this.Dataservs.empApproved.push(this.Dataservs.trade[index])
    //this.Dataservs.trade.splice(index,1)
    const db = getFirestore()
    const colRef = collection(db, 'Schedule')
    const q = query(colRef, where("Date", "==", x));

    const shifts = await getDocs(q);
    let arr=[];

    shifts.docs.map((d) => 
    {

      arr.push({...d.data(), id: d.id})

    });

    console.log(arr);
    // to notice the flow
    this.afs.collection('Schedule').doc(arr[0].id).update
    ({
      //To:'',
      Approve1:true
 
    });
  }
}
