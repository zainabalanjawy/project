import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument} from '@angular/fire/compat/firestore';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { DocumentReference } from '@angular/fire/compat/firestore';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabase, AngularFireDatabaseModule } from '@angular/fire/compat/database';
import firebase from 'firebase/compat/app';
import { signOut } from '@firebase/auth';
import { Auth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';


export interface login{
  email:string,
  password:string
}

export interface User {
  uid?: string,
  password:string,
  email:string,
  firstname:string,
  lastname:string,
  mobile:number,
  role:string
} 


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loginCollectionRef: AngularFirestoreCollection<login>;
  logIn: Observable<login[]>;

  users: Observable<User[]>;
  userCollectionRef: AngularFirestoreCollection<User>;
 

  cur_user: Observable<firebase.User>;
  loggedUser:any;
  public userarr:User[]=[]; 
  public user1;  
  public uid; 
  public disuser:User;
  public admin=true; 
  userRef;
  constructor(
  public afs: AngularFirestore,
  public afa:AngularFireAuth,
  public alertController:AlertController, 
  public router:Router, 
  public db:AngularFireDatabase
  ) 
  {
  this.afa.authState.subscribe((user)=>{
        if(user){ 
         this.loggedUser=user; 
         localStorage.selectItem('user',JSON.stringify(this.loggedUser));
         JSON.parse(localStorage.getItem('user'))

        // this.uid = user.uid;
        this.uid= localStorage.getItem("uid")

        
        this.userCollectionRef = this.afs.collection<User>('User'); 
        
        this.afs.collection('User').doc(this.uid).get().forEach(doc=>{
            this.loggedUser.firstname = doc.get('firstname');
            this.loggedUser.lastname = doc.get('lastname');
            this.loggedUser.email = doc.get('email');
            this.loggedUser.mobile = doc.get('mobile');
            this.loggedUser.role = doc.get('role'); 
        }) 


        //console.log(this.loggedUser)


        
       }
        else
        {
          localStorage.selectItem('user',null);
       }
      }
    )

    this.userCollectionRef = this.afs.collection('User');
    this.users = this.userCollectionRef.valueChanges({idField:'uid'});

     this.uid= localStorage.getItem("uid")

   }



   getSubblier(id: string): Observable<User> {
    return this.userCollectionRef.doc<User>(id).valueChanges().pipe(
    map(res => {
   // res.uid = id;
    return res
    })
    );
    }

setUser(user:any)
{

  const userRef : AngularFirestoreDocument<any> = this.afs.doc(`User/${user.uid}`)
  const userData: User = {
  uid: user.uid,
  password:user.password,
  email:user.email,
  firstname:user.firstname,
  lastname:user.lastname,
  mobile:user.mobile,
  role:user.role
  }

  return userRef.set(userData, {
    merge:true
  })

}



getUser(id: string): Observable<User> {
  return this.userCollectionRef.doc<User>(id).valueChanges().pipe(
  map(user => {
  user.uid = id;
  return user
  })
  );
  }



adduser(user):Promise<any>
{
  return this.userCollectionRef.add(user);
}


  user3: User={} as User;
  /*getSupplierName(index):User
  {
     this.users.subscribe((res) => { 
      console.log(index)
     this.user3.firstname = res[index].firstname;
     this.user3.mobile = res[index].mobile;
     this.user3.lastname = res[index].lastname;
     this.user3.password = res[index].password;
     this.user3.role = res[index].role;

     return this.user3;
   })
   return this.user3;
 }

 */


  updateUser(val:any)
  {

    console.log(val)
    this.userCollectionRef.doc(this.uid).update({
      firstname: val.firstname, 
      lastname: val.lastname,
      email: val.email,
      mobile: val.mobile,
      password: val.password,
      }).then(res=> 
        {
            this.showAlert('Success', 'Profile has been updated successfuly'); 
            this.router.navigateByUrl('/account');
            
        })
        .catch(res=>
          {
            this.showAlert('Faild', 'Please try again!'); 
          }

        ); 
        
  }
  

  get isLoggedIn():boolean
  {
    const user = JSON.parse(localStorage.getItem('user')); 
    return (user !== null)? true:false;
  }

  register(val)
  {
    const email2  =val.email;
    const password2 = val.password;
    console.log(val);
    return this.afa.createUserWithEmailAndPassword(email2, password2);
    //.then(
      doc1=>{
       /* return this.afs.collection('User').doc(doc1.user.uid).set({
          firstname:val.firstname, 
          lastname: val.lastname,
          email:val.email,
          password: val.password,
          mobile: val.mobile,
          role:val.role
        
        });*/ 
      }
     
   // );

  }




  login(email:string,password:string): Promise<any> 
  {
    return this.afa.signInWithEmailAndPassword(email, password);
  }



  logout(): Promise<void> 
  {
    return this.afa.signOut().then(()=>{
      localStorage.removeItem('user')
      localStorage.removeItem('uid')
      
      this.router.navigate(['welcome']);
    });
  }


    deleteAccount() 
    {
        this.userCollectionRef.doc(this.uid).delete();
        let user = firebase.auth().currentUser;
        user.delete().then(res=> {
          localStorage.removeItem('user')
          localStorage.removeItem('uid')
         this.showAlert('Goodbey', 'Account deleted successfuly!'); 
          this.router.navigateByUrl('/welcome');
        }).catch(function(error) {
          this.showAlert('Faild', 'Please try again!'); 
        });
    }

    async showAlert(header,message)
    {
      const alert=await this.alertController.create({
        header, 
        message,
        buttons:['OK'],
  
      });
      await alert.present(); 
    }
  

}
