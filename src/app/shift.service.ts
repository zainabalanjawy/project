import { Injectable } from '@angular/core';

export interface shifts
{
  Date:Date,
  MorningShift:string,
  EveningShift:string,
}

export interface Employees
{
  ID: number,
  Name: string,
  Pic: string
}

export interface Requests
{
  From: Employees[],
  To: Employees[],
  Date:Date,
  reqShift:string,
  theirShift:string
}

@Injectable({
  providedIn: 'root'
})
export class ShiftService {

  constructor() { }

  public Employee: Employees[]=
  [
    {ID: 1 ,Name:"Employe1",Pic:"../../assets/AccountPic.png"},
    {ID: 2 ,Name:"Employe2",Pic:"../../assets/AccountPic1.png"},
    {ID: 3 ,Name:"Employe3",Pic:"../../assets/AccountPic2.png"}
  ]

  public shiftList: shifts[]=
  [
    {Date:new Date('2022-11-11'),MorningShift:"Employe1",EveningShift:""},
    {Date:new Date('2022-11-12'),MorningShift:"Employe2",EveningShift:"Employe3"},
    {Date:new Date('2022-11-13'),MorningShift:"Employe3",EveningShift:"Employe1"}

  ]


  public trade: Requests[]=
  [
    {
     From:[{ID: 1 ,Name:"Employe1",Pic:"../../assets/AccountPic.png"}],
     To :[{ID: 3 ,Name:"Employe3",Pic:"../../assets/AccountPic.png"}],
     Date: new Date('2022-11-13'),
     reqShift: "MorningShift",
     theirShift: "EveningShift"
     
    }
  ]

  public empApproved: Requests[]=
  [
  ]

  public ownerApproved: Requests[]=
  [
  ]


}
