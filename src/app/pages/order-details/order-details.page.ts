import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ActivatedRoute } from '@angular/router';
import { DataService ,order} from 'src/app/data.service';
import { docData } from '@angular/fire/firestore';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.page.html',
  styleUrls: ['./order-details.page.scss'],
})
export class OrderDetailsPage implements OnInit {
  order_id;
  public my_order;   
  public my_order_data;
  
  constructor( private view:ModalController,public afs:AngularFirestore, public auth:AuthService ,public data :DataService) {
    this.my_order= this.afs.collection('User').doc(this.auth.uid).collection('Order').valueChanges();
    this.my_order_data=this.data.getOrder(this.order_id);
   }
  
  ngOnInit() {
    console.log(this.my_order_data['is_fav']);
  
  }

  close(){
    this.view.dismiss()
  }

}
