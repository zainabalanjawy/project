import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService,User } from 'src/app/services/auth.service';
import { AvatarService } from 'src/app/services/avatar.service';
import firebase from 'firebase/compat/app';
import { doc,docData,Firestore, setDoc } from '@angular/fire/firestore';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { getDownloadURL, ref,Storage, uploadString } from '@angular/fire/storage';
import { AlertController, LoadingController } from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export interface Profile
{
username:string; 
role:string; 
}
@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})

export class AccountPage implements OnInit {
  
  profile1={} as Profile; 
  profile:FormGroup;
  public UpdatedUser:User; 

  public usery: User={
    password: '',
    email: '',
    firstname: '',
    lastname: '',
    mobile: 0,
    role: ''
  }; 

  
  // get=this.avatarService.getuserProfile(); 
  constructor(public fb:FormBuilder, public alertCtrl:AlertController, public afs:AngularFirestore , public afAuth:Auth, public alertController:AlertController, public loadingCtrl:LoadingController, public avatarService:AvatarService, public router:Router, public authService:AuthService) { 
    

    this.authService.getUser(this.authService.uid).subscribe(u => {
      this.usery = u;
      });

    console.log(this.usery)
    this.profile = fb.group({
      firstname:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]{1,10}'),Validators.minLength(1),Validators.maxLength(15)])],
      lastname:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]{1,10}'),Validators.minLength(1),Validators.maxLength(15)])],
     // email: ['', Validators.compose([Validators.required,Validators.email])],
      mobile: ['',Validators.compose([Validators.required,Validators.minLength(8)])],
    })
  }


  ngOnInit() 
  {

  }




  public edit: string="create-outline";
  public edit1: string="create-outline";
  public edit2: string="create-outline";

  public isDisabled: boolean=true;
  public isDisabled1: boolean=true;
  public isDisabled2: boolean=true;
  


 async logout()
 {
await this.authService.logout().then((res)=>
  {
    this.showAlert('See you soon', 'You logged out successfuly!'); 
    this.router.navigateByUrl('/welcome');
  }
    )
    .catch((err)=>{this.showAlert('Logout Faild', 'Please try again!')});

 }


  enable()
  {
      if(this.isDisabled)
      {
      this.isDisabled = false;
      this.edit = "checkmark-outline";
      }
      else
      {
        this.isDisabled = true;
        this.edit = "create-outline";
      }

  }

  enable1()
  {
      if(this.isDisabled1)
      {
      this.isDisabled1 = false;
      this.edit1 = "checkmark-outline";
      }
      else
      {
        this.isDisabled1 = true;
        this.edit1 = "create-outline";
      }
  }

  enable2()
  {
      if(this.isDisabled2)
      {
      this.isDisabled2 = false;
      this.edit2 = "checkmark-outline";
      }
      else
      {
        this.isDisabled2 = true;
        this.edit2 = "create-outline";
      }
  }

 
  async showAlert(header,message)
  {
    const alert=await this.alertController.create({
      header, 
      message,
      buttons:['OK'],

    });
    await alert.present(); 
  }



  async showConfirmDelete() {
    let confirm = await this.alertCtrl.create({
    message: 'Are you sure you want to delete this account?',
    buttons: [
    {
    text: 'Yes',
    handler: () => { 
      this.authService.deleteAccount(); 
     }
    },
    {
    text: 'Cancel',
    handler: () => {  }
    }
    ]
    });
    await confirm.present();
    
  }

  async updateProfile(val)
  {
   // if(this.profile.valid)
    //{


      console.log(this.usery)
      console.log(this.UpdatedUser)
      this.usery.mobile = val.mobile;
      //this.usery.email = val.email;
      this.usery.firstname = val.firstname;
      this.usery.lastname = val.lastname;

    this.authService.updateUser(this.usery);

   //}
    
  }
    

}


