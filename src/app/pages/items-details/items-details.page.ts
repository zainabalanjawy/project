import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { DataService, Item} from 'src/app/data.service';
import { AuthService,User } from 'src/app/services/auth.service';
declare var dynamics: any;
@Component({
  selector: 'app-items-details',
  templateUrl: './items-details.page.html',
  styleUrls: ['./items-details.page.scss'],
})
export class ItemsDetailsPage implements OnInit {
  myId = null;
  public item; 
  parsedDate;
  segment:string="";
  activeVariation: string;
  roleMessage = '';
  subName;
  user1CollectionRef: AngularFirestoreCollection<User>;
  users1: Observable<User[]>;
  uid1;
  subblier: User =
  {
    uid: "",
    password:"",
    email:"",
    firstname:"",
    lastname:"",
    mobile:0,
    role:""
  }

  constructor( public alertController:AlertController, public auth:AuthService, public afs:AngularFirestore , private alertCtrl:AlertController, private data: DataService, private activatedRoute: ActivatedRoute) {
 



      console.log(this.subblier)
   
  }



  ngOnInit() {
   this.item = {} as Item;
    this.myId = this.activatedRoute.snapshot.paramMap.get('i');
    this.item = this.data.getItemId(this.myId);
    console.log(this.item) 
    this.uid1=this.auth.uid; 
    console.log(this.uid1) 
    console.log(this.item)

 /*   this.afs.collection('User').doc(this.item.SubID).valueChanges().subscribe((user:any)=>{
      console.log(user); 
      this.item.Supplier=user.firstname;});

*/
   
    //this.subName = this.auth.getSupplierName(this.item.SubID);
   
  this.activeVariation = 'quantity';

  }
  

getName()
{
  console.log(this.item.SubID)
  return this.item.SubID;

}


  incrementQty() {
    if(this.item[this.myId].thershold >1)
    this.item[this.myId].thershold+= 1;
  }

  decrementQty() {
    if(this.item[this.myId].thershold >1)
    this.item[this.myId].thershold -= 1;
  }


  user3: User={} as User;
 /* getSupplierName(index):User
  {

    */ 
  
   /*}*/

  async AddQuantity(){
    const msgBox = await this.alertCtrl.create({
     header: 'New Order',
     message: 'Enter quantity of selected item.',
     inputs:[{ name:'name',
      type: 'number',
     placeholder: 'Quantity',
     min: 1,
     max: 100}],
     buttons: [
      {
        text:'ok',
        role: 'confirm',
         handler: (res)=>{
          if(res.name!='')
          {
            if(res.name<100 && res.name>0){
              this.item.orderQTY=res.name; 
              this.data.addCart(this.item,this.uid1).then(
                (r)=>{
                  this.showAlert("Success","Item added successfuly!");}
              ).catch(
                (err)=>{
                  this.showAlert("Failure", "Item not added to cart");
              }
              )
            

            }

            else
            this.showAlert("Error",'Quantity is not allowed!');

          }
          else
          alert('Enter a quantity of selected item to continue ordering!');
          }
        
      },

      {
        text:'cancel',
        role: 'cancel',
         handler: (data)=>{
         }
      }
      
      ]
    });

   
    await msgBox.present(); 
    const { role } = await msgBox.onDidDismiss()
    this.roleMessage = `Dismissed with role: ${role}`;
  
     }



    /* dynamics.animate(elem, {translateX:  350,scale:1, opacity:1}, {type:   dynamics.spring,
      frequency:   200,
      friction:  200,
      duration: 5000,});
    */

      async showAlert(header,message)
      {
        const alert=await this.alertController.create({
          header, 
          message,
          buttons:['OK'],
    
        });
        await alert.present(); 
      }


}
