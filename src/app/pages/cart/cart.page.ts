import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { exists } from 'fs';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DataService,Item, order } from 'src/app/data.service';
import { AuthService } from 'src/app/services/auth.service';
declare var dynamics: any;
@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})


export class CartPage implements OnInit {
  private btnAnimate: Boolean = false;
  public items1 = [];
  public myId;
  public viewCart;
  public subArray=[]; 
  activeVariation: string; 
  public orderArray:Item[]=[];
  public order1:order; 
  constructor(public navCtrl: NavController, public platform: Platform, public alertController:AlertController, public afs:AngularFirestore,public data: DataService, public auth:AuthService) { 
    this.viewCart = this.afs.collection('User').doc(this.auth.uid).collection('Cart').valueChanges();
    this.order1 = {} as order;
  
    this.viewCart.forEach(e => {
      console.log(e)

    this.data.total=0; 
    for(let i=0;i<e.length;i++){
    this.orderArray.push(e[i]);
    this.data.total+= Number(e[i].orderQTY)*Number(e[i].price); 
    }


    });


  }

  ngOnInit() {
    this.activeVariation = 'quantity';
  
  }


  


      
  
  




  async showAlert(header,message)
  {
    const alert=await this.alertController.create({
      header, 
      message,
      buttons:['OK'],

    });
    await alert.present(); 
  }


  incrementQty(myId) {
    if( this.data.itemsToOrder[myId].orderQTY>1)
    this.data.itemsToOrder[myId].orderQTY+=1;
  }

  decrementQty(myId) {
    if( this.data.itemsToOrder[myId].orderQTY>1)
    this.data.itemsToOrder[myId].orderQTY -=1; 
  }


  
 /* placeorder()
  {
   
       let today = new Date().toISOString();
       let exists = false; 
       this.viewCart.forEach(e => {
        console.log(e)
        e.forEach(element => {
      
          console.log( element.price)
       
        //empty orders for subbliers
        console.log('which',e)
        if(this.subArray.length==0)
        {
          this.subArray.push(element.SubID);
          this.data.total=0; 
          

          // create a new order for this subblier 
      let updatedCartoon=Math.ceil(Number(element.orderQTY)/Number(element.ipCartoon));
      this.afs.collection('items').doc(element.id).update({
        Date: today, 
        Cartoon: updatedCartoon
      })
      console.log('updatedCartoon',updatedCartoon)


      this.afs.collection('User').doc(this.auth.uid).collection('Cart').doc(element.id).update({
        Date: today, 
        Cartoon: updatedCartoon
      })

      this.order1.id=element.SubID;
      this.order1.total = Number(element.orderQTY)*Number(element.price);
      this.order1.lastAdded = element; 
      this.order1.items1=element;
      this.order1.is_fav = false;
      this.order1.status = 'placed';
      console.log('order1',this.order1)
      console.log('ordersubarray',this.subArray)
      //this.data.createOrder(this.order1)

        }

         //subblier order array not empty 
        else
        {  
          // this subblier has an order 
           this.subArray.find((check =>
            {
              if (check == element.SubID)
              {
                exists=true; 
                console.log('found')
                // update his order 
                this.data.total=0; 
                
      
                // create a new order for this subblier 
            let updatedCartoon=Math.ceil(Number(element.orderQTY)/Number(element.ipCartoon));
            this.afs.collection('items').doc(element.id).update({
              Date: today, 
              Cartoon: updatedCartoon
            })
            console.log('updatedCartoon',updatedCartoon)
      
      
            this.afs.collection('User').doc(this.auth.uid).collection('Cart').doc(element.id).update({
              Date: today, 
              Cartoon: updatedCartoon
            })
      
            console.log(element.SubID)
            this.order1.id=element.SubID;
            this.order1.total+= Number(element.orderQTY)*Number(element.price);
            this.order1.lastAdded = element; 
            this.order1.items1=element;
            console.log('order2',this.order1)
            
          // this.data.updateOrder(this.order1)


              }
              // this subblier has no order 
              else
              {
                console.log('notfound')
                exists=false; 
              
       
              } 
            })); 
          }
        });
  }); 
        
      
       
     













          }*/





  placeorder()
  {
   
       let today = new Date().toISOString();
       this.viewCart.forEach(e => {
        this.data.total=0; 
        console.log(e)
          for(let i=0;i<e.length;i++){
            let updatedCartoon=Math.ceil(Number(e[i].orderQTY)/Number(e[i].ipCartoon));
            this.afs.collection('items').doc(e[i].id).update({
              Date: today, 
              Cartoon: updatedCartoon
            })


            this.afs.collection('User').doc(this.auth.uid).collection('Cart').doc(e[i].id).update({
              Date: today, 
              Cartoon: updatedCartoon
            })
            console.log(this.order1)
    
          
          }
        });
       this.order1.total = this.data.total;
       this.order1.items1 = this.orderArray;
       this.order1.is_fav = false;
       this.order1.status = 'placed';
       this.data.placeOrder(this.order1).then((res)=>{
       this.showAlert("Success","Order placed successfuly!");
       this.data.deleteCart(); 

     
        })
        .catch((e)=>{
          this.showAlert("Faild","Error while placed this ordere, please try again!");
      })
      
    
  }


        }
