import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials: FormGroup; 
  constructor(
    public fb: FormBuilder, 
    private authService:AuthService, 
    private loadingController:LoadingController, 
    private alertController:AlertController,
    private router:Router, 
    public modalCtrl: ModalController)
    {
      /*if(this.authService.uid!=null){
        this.showAlert("Warning","You are already loggedn!");

    
      }*/ 
  }


  ngOnInit() {
    this.credentials = this.fb.group({
      email: ['', Validators.compose([Validators.required,Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8)])]
      });
  }

    login(val)
    {
       if(this.credentials.valid)
       {
         this.authService.login(val.email,val.password)
         .then((res)=>
         { 
            localStorage.setItem('uid', res.user.uid)
          this.showAlert('Welcome back', 'You logged in successfuly!'); 
          this.dismiss();
          if(this.authService.loggedUser.role == 'Admin')
          {this.router.navigate(['/items'])}
          else 
          if(this.authService.loggedUser.role == 'Employee')
          {this.router.navigate(['/trade'])}
          
        }).catch((err)=>{this.showAlert('Login Faild', 'Please try again!')});

       } 
    } 


  async showAlert(header,message)
  {
    const alert=await this.alertController.create({
      header, 
      message,
      buttons:['OK'],

    });
    await alert.present(); 
  }

  async dismiss() 
  {
    await this.modalCtrl.dismiss();
  }



}
