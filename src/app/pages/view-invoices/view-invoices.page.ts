import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OrderDetailsPage } from '../order-details/order-details.page';

@Component({
  selector: 'app-view-invoices',
  templateUrl: './view-invoices.page.html',
  styleUrls: ['./view-invoices.page.scss'],
})
export class ViewInvoicesPage implements OnInit {
  public list;
  constructor(public modalCtrl:ModalController) { }

  ngOnInit() {
    this.list=[
      {id:"cD4144",is_fav:false},
      {id:"XGE3742",is_fav:true},
      {id:"cD2341",is_fav:false},
      {id:"cwer9745",is_fav:false},
      {id:"cD2341",is_fav:false},
    ];
  }
  

  async goOrderDetails(){
    const modal = await this.modalCtrl.create({
      component: OrderDetailsPage,
      animated: true,
      mode: 'ios',
      backdropDismiss: false,
  
    })
    return await modal.present();
  }

}
