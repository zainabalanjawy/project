import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewInvoicesPageRoutingModule } from './view-invoices-routing.module';

import { ViewInvoicesPage } from './view-invoices.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewInvoicesPageRoutingModule
  ],
  declarations: [ViewInvoicesPage]
})
export class ViewInvoicesPageModule {}
