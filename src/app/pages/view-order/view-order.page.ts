import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { PastOrdersPage } from '../past-orders/past-orders.page';
import { FavoriteOrdersListPage } from '../favorite-orders-list/favorite-orders-list.page';
import { OrderDetailsPage } from '../order-details/order-details.page';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthService } from 'src/app/services/auth.service';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.page.html',
  styleUrls: ['./view-order.page.scss'],
})
export class ViewOrderPage implements OnInit {
favo:string;
public list;
public past_orders;
public getorders;
  //1 define arrays 
  private orders2;
  constructor( private modalCtrl:ModalController,public afs:AngularFirestore,public auth:AuthService, public data:DataService) { 
    this.past_orders = this.afs.collection('User').doc(this.auth.uid).collection('Order',ref => ref.where('status','==',"placed")).valueChanges();
    this.past_orders.forEach(e=>{
      this.getorders=e;
      console.log(e)
    })

    
    

  }

  ngOnInit() {

  }


  public passedArray;  
  changestatus(i)
  {
    this.passedArray=this.getorders[i]; 
    this.data.updatestatus(this.passedArray);
  }

clickFavorates(index){
  if(this.list[index].is_fav==false)
  this.list[index].is_fav=true;
  else if(this.list[index].is_fav==true)
  this.list[index].is_fav=false;
}

async goPastOrder(){
  const modal = await this.modalCtrl.create({
    component: PastOrdersPage,
    animated: true,
    mode: 'ios',
    backdropDismiss: false,

  })
  return await modal.present();
}


async goFavorateOrders(){
  const modal = await this.modalCtrl.create({
    component: FavoriteOrdersListPage,
    animated: true,
    mode: 'ios',
    backdropDismiss: false,

  })
  return await modal.present();


}



async goOrderDetails(id){
  const modal = await this.modalCtrl.create({
    component: OrderDetailsPage,
    componentProps: { 
      order_id: id,
    },
    animated: true,
    mode: 'ios',
    backdropDismiss: false,

  })
  return await modal.present();
}

}
