import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-history-orders',
  templateUrl: './history-orders.page.html',
  styleUrls: ['./history-orders.page.scss'],
})
export class HistoryOrdersPage implements OnInit {
  public viewItems;
  c=0; 
  f=0;
  d=0; 
  constructor(public afs:AngularFirestore, public modalCtrl:ModalController, public data:DataService) {
    this.viewItems = this.afs.collection('items').valueChanges();
    
    
    this.viewItems.forEach(e => {
      console.log(e)
      for(let i=0;i<e.length;i++){
        if(e[i].type1=='Chips & Snacks')
        this.c++; 
    
        else if (e[i].type1=='Fruits & Vegetables')
        this.f++; 
    
        else if (e[i].type1=='Diary & Beverages')
        this.d++; 
        }

   
     
    }
  );


  
   }
  googleChartLibrary;
  chart='';
  private items2;
  ngOnInit() {
    this.useVanillaJSLibrary();
    
      //2 retrive items


       
       this.items2.forEach(e => {
       
        if(e.type1=='Chips & Snacks')
        this.c++; 
    
        else if (e.type1=='Fruits & Vegetables')
        this.f++; 
    
        else if (e.type1=='Diary & Beverages')
        this.d++; 
    
    
      
       });
       console.log('d',this.d)
  }
    

  


 

  cancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }



  useVanillaJSLibrary() {
    this.googleChartLibrary = (<any>window).google;
    // Load the Visualization API and the corechart package.
    this.googleChartLibrary.charts.load('current', { 'packages': ['corechart'] });

    // Set a callback to run when the Google Visualization API is loaded.
    this.googleChartLibrary.charts.setOnLoadCallback(this.drawChart.bind(this));
    this.googleChartLibrary.charts.setOnLoadCallback(this.drawLine.bind(this));
  }



  drawChart () {
    // Create the data table.
    var data = new this.googleChartLibrary.visualization.DataTable();
    data.addColumn('string', 'Product Name');
    data.addColumn('number', 'Number');
    data.addRows([
      ['Strawberry', this.f],
      ['Lays barbecue', this.c],
      ['Actievia yougurt', this.d],
    ]);

    var options = {
      
      width: 400, 
      height: 400, 
      slices: {
      0: {color:'#5EC5D4' },
      1: {color:'#926bac' },
      2: {color:'#F6F7A9'},
      3: {color:'#0030b2'},
      4: {color:'#080149'}    
    }
  
  }

    // Instantiate and draw our chart, passing in some options.
    var chart = new this.googleChartLibrary.visualization
      .PieChart(document.getElementById('pie-chart-div'));

    chart.draw(data, options,{
      'title': 'Most Selling Product',
      'width': 1500,
      'height': 600
    });
  }


  drawLine () {
    // Create the data table.
    var data = new this.googleChartLibrary.visualization.DataTable();
    data.addColumn('string', 'Month');
    data.addColumn('number', 'Number');
    data.addRows([
      ['June', 8 ],
      ['July', 10],
      ['August', 7],
    ]);

    var options = {
      
      width: 355, 
      height: 400, 
      slices: {
      0: {color:'#5EC5D4' },
      1: {color:'#926bac' },
      2: {color:'#F6F7A9'},
    }
  
  }

    

    // Instantiate and draw our chart, passing in some options.
    var chart = new this.googleChartLibrary.visualization
      .ColumnChart(document.getElementById('line-chart-div'));

    chart.draw(data, options,{
      'width': 800,
      'height': 600
    });
  }



}
