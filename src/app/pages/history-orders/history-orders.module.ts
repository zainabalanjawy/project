import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoryOrdersPageRoutingModule } from './history-orders-routing.module';

import { HistoryOrdersPage } from './history-orders.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HistoryOrdersPageRoutingModule
  ],
  declarations: [HistoryOrdersPage]
})
export class HistoryOrdersPageModule {}
