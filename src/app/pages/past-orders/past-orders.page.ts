import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-past-orders',
  templateUrl: './past-orders.page.html',
  styleUrls: ['./past-orders.page.scss'],
})
export class PastOrdersPage implements OnInit {
favo:string;
public list;
public past_order_list;
  constructor(private modalCtrl:ModalController,public afs:AngularFirestore,public auth:AuthService) {
    this.past_order_list=this.afs.collection('User').doc(auth.uid).collection("Order").valueChanges();
   }

  ngOnInit() {
    this.favo="heart-outline";
    this.list=[
      {id:"cD4144",is_fav:false},
      {id:"XGE3742",is_fav:true},
      {id:"cD2341",is_fav:false},
      {id:"cwer9745",is_fav:false},
      {id:"cD2341",is_fav:false},
    ];
  }

clickFavorates(index){
  if(this.list[index].is_fav==false)
  this.list[index].is_fav=true;
  else if(this.list[index].is_fav==true)
  this.list[index].is_fav=false;


}
async close(){
  this.modalCtrl.dismiss();
}
}
