import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TradeReqPage } from './trade-req.page';

describe('TradeReqPage', () => 
{
  let component: TradeReqPage;
  let fixture: ComponentFixture<TradeReqPage>;

  beforeEach(waitForAsync(() => 
  {
    TestBed.configureTestingModule
    ({
      declarations: [ TradeReqPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TradeReqPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
