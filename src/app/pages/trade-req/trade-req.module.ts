import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TradeReqPageRoutingModule } from './trade-req-routing.module';

import { TradeReqPage } from './trade-req.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TradeReqPageRoutingModule
  ],
  declarations: [TradeReqPage]
})
export class TradeReqPageModule {}
