import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-trade-req',
  templateUrl: './trade-req.page.html',
  styleUrls: ['./trade-req.page.scss'],
})
export class TradeReqPage implements OnInit {

  @Input() ID: number; 
  
  uid;
  users$;
  schedual$;

  constructor(public ModalCtrl:ModalController ,public router:Router, public navParams:NavParams ,public Dataservs:DataService,public activatedRouat:ActivatedRoute,  private popoverController: PopoverController)
   { 
    this.uid = localStorage.getItem("uid")
    this.users$ = this.Dataservs.getuser();
    this.schedual$ = this.Dataservs.getschedule();
   }

  ngOnInit()
  {

  }

  async DismissClick() 
  {
    await this.popoverController.dismiss();
  }

  approve(index)
  {
  
    // to notice the flow
    this.router.navigate(['/tradereq']);
  }

  
  cancel() 
  {
    return this.ModalCtrl.dismiss(null, 'cancel');
  }

}
