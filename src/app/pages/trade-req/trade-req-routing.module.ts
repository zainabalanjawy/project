import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TradeReqPage } from './trade-req.page';

const routes: Routes = [
  {
    path: '',
    component: TradeReqPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TradeReqPageRoutingModule {}
