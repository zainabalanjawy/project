import { Component,Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { DataService } from 'src/app/data.service';
import { ShiftService } from 'src/app/shift.service';


@Component({
  selector: 'app-empsch',
  templateUrl: './empsch.page.html',
  styleUrls: ['./empsch.page.scss'],
})
export class EmpschPage implements OnInit {
 
  users$;
  schedual$;
  @Input() ID: number; 

  
  constructor(public ModalCtrl:ModalController,public Dataservs:DataService,public activatedRouat:ActivatedRoute, navParams: NavParams)
  {
    this.users$ = this.Dataservs.getuser();
    this.schedual$ = this.Dataservs.getschedule();
  }

  ngOnInit() 
  {   }

  cancel() 
  {
    return this.ModalCtrl.dismiss(null, 'cancel');
  }

}
