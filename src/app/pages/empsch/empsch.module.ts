import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmpschPageRoutingModule } from './empsch-routing.module';

import { EmpschPage } from './empsch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmpschPageRoutingModule
  ],
  declarations: [EmpschPage]
})
export class EmpschPageModule {}
