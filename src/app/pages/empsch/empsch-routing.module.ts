import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmpschPage } from './empsch.page';

const routes: Routes = [
  {
    path: '',
    component: EmpschPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmpschPageRoutingModule {}
