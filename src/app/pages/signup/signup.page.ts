import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Auth } from '@angular/fire/auth';
import { doc, Firestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';
import { AuthService,User } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  credentials: FormGroup; 
  public view='name';
  public SignedUser:User;
  constructor(
    public fb: FormBuilder, 
    public authService:AuthService,
    public loadingController:LoadingController, 
    public alertController:AlertController,
    public router:Router, 
    public modalCtrl: ModalController
    )
    {
      this.SignedUser = {} as User;
      this.credentials = fb.group({
        firstname:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]{1,10}'),Validators.minLength(1),Validators.maxLength(15)])],
        lastname:['',Validators.compose([Validators.required,Validators.pattern('[a-zA-Z]{1,10}'),Validators.minLength(1),Validators.maxLength(15)])],
        email: ['', Validators.compose([Validators.required,Validators.email])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(8)])], 
        mobile: ['',Validators.compose([Validators.required,Validators.minLength(8)])],
        role:['',Validators.compose([Validators.required])]
      });
     }

  ngOnInit() {

  }


  async register(val) 
  {
  
    if(this.credentials.valid)
    {
      this.SignedUser.password = val.password;
      this.SignedUser.mobile = val.mobile;
      this.SignedUser.email = val.email;
      this.SignedUser.firstname = val.firstname;
      this.SignedUser.lastname = val.lastname;
      this.SignedUser.role = val.role;
      this.authService.register(this.SignedUser).then((res)=>{
      
        const userData: User = {
          uid: res.user.uid,
          password:val.password,
          email:val.email,
          firstname:val.firstname,
          lastname:val.lastname,
          mobile:val.mobile,
          role:val.role
          }
      
      this.authService.setUser(userData); 
      this.showAlert('Welcome', 'You registered successfuly!'); 
      this.dismiss();
      this.router.navigateByUrl('/account');

      localStorage.setItem('uid', res.user.uid)
      localStorage.getItem('uid')
    })
    .catch((err)=>{
      this.showAlert('Error', 'Registeration faild!'); 
    })
   ; 

    }



  }


  async showAlert(header,message)
  {
    const alert=await this.alertController.create({
      header, 
      message,
      buttons:['OK'],

    });
    await alert.present(); 
  }


  async dismiss() {
    return await this.modalCtrl.dismiss();
  }


}
