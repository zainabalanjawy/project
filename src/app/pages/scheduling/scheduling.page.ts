import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AssignshiftPage } from '../assignshift/assignshift.page';
import { TradereqPage } from '../tradereq/tradereq.page';
import { ViewshiftPage } from '../viewshift/viewshift.page';
import { EmpschPage } from 'src/app/pages/empsch/empsch.page';
import { DataService } from 'src/app/data.service';
import { AngularFirestore} from '@angular/fire/compat/firestore';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { ShiftService } from 'src/app/shift.service';

@Component({
  selector: 'app-scheduling',
  templateUrl: './scheduling.page.html',
  styleUrls: ['./scheduling.page.scss'],
})

export class SchedulingPage implements OnInit {

  users$;
  filteredList = [];
  isfiltered: boolean ;
  
  ud:string;

  constructor(public data:DataService ,public Dataservs:ShiftService, public ModalCtrl: ModalController) 
  {
    this.users$ = this.data.getuser();

  }

  ngOnInit() 
  { }

 
  

  async EmolyeeSche(id)
  {
    const modal = await this.ModalCtrl.create
    ({ component: EmpschPage,
      componentProps: 
      {'ID': id }
    }); 
    return await modal.present(); 
  }


  async presentModal()
  {
    const modal = await this.ModalCtrl.create
    ({ component: AssignshiftPage }); 
    return await modal.present(); 
  }

  async presentModal1()
  {
    const modal = await this.ModalCtrl.create
    ({ component: ViewshiftPage }); 
    return await modal.present(); 
  }

  async presentModal2()
  {
    const modal = await this.ModalCtrl.create
    ({  component: TradereqPage, }); 
    return await modal.present(); 
  }


}
