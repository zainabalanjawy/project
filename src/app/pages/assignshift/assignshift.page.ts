import { Component, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonSelect, ModalController, NavController, PopoverController } from '@ionic/angular';
import { DataService } from 'src/app/data.service';
import { ShiftService } from 'src/app/shift.service';
import { getApp } from 'firebase/app';
import { doc,setDoc,addDoc, collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/compat/database';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { collectionChanges, collectionData, Firestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


export interface shifts
{
  Date:Date,
  Shift1:string,
  Shift2:string,
}

@Component({
  selector: 'app-assignshift',
  templateUrl: './assignshift.page.html',
  styleUrls: ['./assignshift.page.scss'],
})



export class AssignshiftPage implements OnInit 
{
 
  shiftCollection: AngularFirestoreCollection;


  shiftid = null;
  users$;
  schedual$;
  assignedEmpl;
  shift;
  log; emp ; datee; indx:number;
  display= false;
  noneSelected;
  
 public suID;
  

  @ViewChild('mySelect', { static: false }) selectRef: IonSelect;

  
 
  constructor(public firestore:Firestore, public afs:AngularFirestore  , public navCtrl:NavController, public Dataservs:DataService ,public ModalCtrl:ModalController,public alrtctr:AlertController, private popoverController: PopoverController) 
  {
    
    this.shiftCollection = this.afs.collection('Schedual');

    this.users$ = this.Dataservs.getuser();
    this.schedual$ = this.Dataservs.getschedule();

  }
    
  


  ngOnInit() 
  {

  }


    
  async openSelect() 
  {
    this.selectRef.open();
  }

  shifts()
  {
    this.display = !this.display;
  }
  

 async Assigntoshift(i) 
  {
    let partYouWant;
    const dateArray = this.datee.split('T');
    if (dateArray.length > 0)
    {
      partYouWant = dateArray[0];
    }


    const db = getFirestore()
    const colRef = collection(db, 'Schedule')

  

    const q = query(colRef, where("Date", "==", partYouWant));
    const shifts = await getDocs(q);
    let arr=[];

    console.log(arr)
    shifts.docs.map((d) => 
    {

      arr.push({...d.data(), id: d.id})

    });


    if(arr.length == 0)
    {
      if(this.shift == 'Shift1')
      {
        await setDoc(doc(db, "Schedule",'000'), 
        {
          Approve1:false,
          Approve2:false,
          Date: partYouWant,
          Shift1: i,
          Shift2:'',
          To:'' 
        });
      }

      if(this.shift == 'Shift2')
      {
        await setDoc(doc(db, "Schedule",'999'), 
        {
          Approve1:false,
          Approve2:false,
          Date: partYouWant,
          Shift1: '',
          Shift2:i,
          To:'' 
        });
      }

    }


    if(arr.length != 0)
    {
      if(this.shift == 'Shift1')
      {
        this.afs.collection('Schedule').doc(arr[0].id).update
        ({
          Shift1:i
        });
      }
    

      if(this.shift == 'Shift2')
      { 
        this.afs.collection('Schedule').doc(arr[0].id).update
        ({
          Shift2:i
        });
      }
    }

  }  



   

  async DismissClick() 
  {
    await this.popoverController.dismiss();
    
  }
 
  cancel() 
  {
    return this.ModalCtrl.dismiss(null, 'cancel');
  }
  
  
    
    
}
