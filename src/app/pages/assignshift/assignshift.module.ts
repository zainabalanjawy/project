import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AssignshiftPageRoutingModule } from './assignshift-routing.module';

import { AssignshiftPage } from './assignshift.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AssignshiftPageRoutingModule
  ],
  declarations: [AssignshiftPage]
})
export class AssignshiftPageModule {}
