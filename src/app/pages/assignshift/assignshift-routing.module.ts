import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AssignshiftPage } from './assignshift.page';

const routes: Routes = [
  {
    path: '',
    component: AssignshiftPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AssignshiftPageRoutingModule {}
