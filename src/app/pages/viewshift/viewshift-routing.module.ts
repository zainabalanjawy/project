import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewshiftPage } from './viewshift.page';

const routes: Routes = [
  {
    path: '',
    component: ViewshiftPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewshiftPageRoutingModule {}
