import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/data.service';
import { ShiftService } from 'src/app/shift.service';

@Component({
  selector: 'app-viewshift',
  templateUrl: './viewshift.page.html',
  styleUrls: ['./viewshift.page.scss'],
})
export class ViewshiftPage implements OnInit {

  users$;
  schedual$;

  constructor(public Dataservs:DataService ,public ModalCtrl:ModalController) 
  {
    this.users$ = this.Dataservs.getuser();
    this.schedual$ = this.Dataservs.getschedule();
  }

  segment = "Assign";
  ngOnInit() {
  }

  cancel()
  {
    return this.ModalCtrl.dismiss(null, 'cancel');
  }



}

