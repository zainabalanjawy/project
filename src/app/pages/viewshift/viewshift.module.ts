import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewshiftPageRoutingModule } from './viewshift-routing.module';

import { ViewshiftPage } from './viewshift.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewshiftPageRoutingModule
  ],
  declarations: [ViewshiftPage]
})
export class ViewshiftPageModule {}
