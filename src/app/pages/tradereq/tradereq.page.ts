import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { ShiftService } from 'src/app/shift.service';
import { DataService } from 'src/app/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { collection, getDocs, getFirestore, query, where } from 'firebase/firestore';


@Component({
  selector: 'app-tradereq',
  templateUrl: './tradereq.page.html',
  styleUrls: ['./tradereq.page.scss'],
})
export class TradereqPage implements OnInit {
 
  
    uid;
    users$;
    schedual$;
  
    constructor(public afs:AngularFirestore,public Dataservs:DataService, public router:Router)
    { 
      this.uid = localStorage.getItem("uid")
      
      this.users$ = this.Dataservs.getuser();
      this.schedual$ = this.Dataservs.getschedule();
  
  
    }
  
    ngOnInit() 
    {
  
    }
  
  
    pages =
    [
      {
        title: 'Acount',
        url: '/account',
        icon: 'person'
      },
      {
        title: 'sign-out',
        url: '/account',
        icon: 'log-out'
      }
    ];
  
    async approve(x,one,two)
    {
      //this.Dataservs.empApproved.push(this.Dataservs.trade[index])
      //this.Dataservs.trade.splice(index,1)
      const db = getFirestore()
      const colRef = collection(db, 'Schedule')
      const q = query(colRef, where("Date", "==", x));
  
      const shifts = await getDocs(q);
      let arr=[];
  
      shifts.docs.map((d) => 
      {
  
        arr.push({...d.data(), id: d.id})
  
      });
  
           
    
        this.afs.collection('Schedule').doc(arr[0].id).update
        ({
          To:'', 
          Approve2:true,
          Shift1:two,
          Shift2:one
        });
 


    }
  }
  