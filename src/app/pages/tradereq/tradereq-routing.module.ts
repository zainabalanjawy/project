import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TradereqPage } from './tradereq.page';

const routes: Routes = [
  {
    path: '',
    component: TradereqPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TradereqPageRoutingModule {}
