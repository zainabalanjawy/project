import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TradereqPageRoutingModule } from './tradereq-routing.module';

import { TradereqPage } from './tradereq.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TradereqPageRoutingModule
  ],
  declarations: [TradereqPage]
})
export class TradereqPageModule {}
