import { Component, OnInit } from '@angular/core';
import { AngularFirestore} from '@angular/fire/compat/firestore';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { first } from 'rxjs/operators';
import { DataService, Item } from 'src/app/data.service';
import {Observable}from 'rxjs';
declare var dynamics: any;


@Component({
  selector: 'app-items',
  templateUrl: './items.page.html',
  styleUrls: ['./items.page.scss'],
})
export class ItemsPage implements OnInit {
  private btnAnimate: Boolean = false;
  public items1 = [];
  public itemsBackup=[]; 
  public type1 = [];
  searchQuery: string = ''; 
  public c=0; 
  public f=0; 
  public d=0; 

  //1 define arrays 
  private items2;
  private item : Item = {} as Item;
  

  public categories:any[]=[];



  constructor( public alertController:AlertController, public afs:AngularFirestore,  public alertCtrl:AlertController, public data: DataService, public activatedRoute: ActivatedRoute) {

   
   }

 async ngOnInit() {
   // this.items1 = this.data.getItems();
   // this.type1 = this.data.type2;

    //2 retrive items
   this.data.getItems().subscribe(
    res=>{
      this.items2=res; 
      this.itemsBackup=res;
 
    }
   );
    

    //this.mylist = await this.createItems();
  }


  const  = this.afs.collection('items');
  /*  insert(){
        this.data.addItem1(this.item).then( (response)=>{
            alert("Item Inserted Successfully");
            this.item= {} as Item;
          });
        }
*/ 
    
animateCard(){
  var el = document.getElementById("myCard")
  if (!this.btnAnimate) {
  this.btnAnimate = true;
  dynamics.animate(el, {
  translateY: 0, translateX:500
  }, {
  type: dynamics.spring,
  duration: 1300,
  complete: () => { this.btnAnimate = false; }
  });
  }
  }

 /* async createItems(): Promise<any>{
    const mylist  = await this.afs.collection('items').valueChanges().pipe(first()).toPromise();
    this.mylistBackup = mylist;
    return mylist;
   }
   */ 



   async showAlert(header,message)
   {
     const alert=await this.alertController.create({
       header, 
       message,
       buttons:['OK'],
 
     });
     await alert.present(); 
   }

   
   async filter(ev:any){
    this.items2=this.itemsBackup;
    
    let val = ev.target.value;
  
    if (val && val.trim() != '') {
      this.items2 = this.items2.filter((item) => {
      return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }


    console.log(ev);
      
  }
}
