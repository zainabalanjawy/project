import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-favorite-orders-list',
  templateUrl: './favorite-orders-list.page.html',
  styleUrls: ['./favorite-orders-list.page.scss'],
})
export class FavoriteOrdersListPage implements OnInit {
  favo:string;
  public list;
  public fav_order;
    constructor(private modalCtrl:ModalController,public afs:AngularFirestore,public auth:AuthService) {
      this.fav_order = this.afs.collection('User').doc(this.auth.uid).collection('Order',ref => ref.where('is_fav','==',true)).valueChanges();
      
     }
  
    ngOnInit() {
      this.favo="heart-outline";
      this.list=[
        {id:"cD4144",is_fav:true},
        {id:"XGE3742",is_fav:true},
        {id:"cD2341",is_fav:true},
      ];
    }
  
  clickFavorates(index){
 if(this.list[index].is_fav==true)
    this.list.splice(this.list[index],1);
  
  }

async close() {
    return await this.modalCtrl.dismiss();
  }
}
