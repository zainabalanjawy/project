import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavoriteOrdersListPage } from './favorite-orders-list.page';

const routes: Routes = [
  {
    path: '',
    component: FavoriteOrdersListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavoriteOrdersListPageRoutingModule {}
