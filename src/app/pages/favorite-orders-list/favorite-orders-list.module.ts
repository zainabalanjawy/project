import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavoriteOrdersListPageRoutingModule } from './favorite-orders-list-routing.module';

import { FavoriteOrdersListPage } from './favorite-orders-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavoriteOrdersListPageRoutingModule
  ],
  declarations: [FavoriteOrdersListPage]
})
export class FavoriteOrdersListPageModule {}
