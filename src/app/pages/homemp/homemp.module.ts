import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomempPageRoutingModule } from './homemp-routing.module';

import { HomempPage } from './homemp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomempPageRoutingModule
  ],
  declarations: [HomempPage]
})
export class HomempPageModule {}
