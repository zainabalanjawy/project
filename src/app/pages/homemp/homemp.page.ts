import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ShiftService } from 'src/app/shift.service';
import { TradeReqPage } from '../trade-req/trade-req.page';
import { AuthService,User } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup ,Validators} from '@angular/forms';



@Component({
  selector: 'app-homemp',
  templateUrl: './homemp.page.html',
  styleUrls: ['./homemp.page.scss'],
})



export class HomempPage implements OnInit {
  uid;
  
  constructor(public fb:FormBuilder,public auth:AuthService,public ModalCtrl:ModalController ,public Dataservs:ShiftService,public activatedRouat:ActivatedRoute)
  { 
    this.uid = localStorage.getItem("uid")
  
  }
 

  ngOnInit()
  {

  }



  pages = 
  [
    {
      title: 'Acount',
      url: '/account',
      icon: 'person'
    },
    {
      title: 'sign-out',
      url: '/account',
      icon: 'log-out'
    }
  ];

}
