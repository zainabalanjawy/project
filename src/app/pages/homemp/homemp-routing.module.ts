import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomempPage } from './homemp.page';

const routes: Routes = [
  {
    path: '',
    component: HomempPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomempPageRoutingModule {}
