import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { ActivatedRoute } from '@angular/router';
import { ActionSheetController, AlertController, ModalController } from '@ionic/angular';
import { DataService, Item } from 'src/app/data.service';
import { HistoryOrdersPage } from '../history-orders/history-orders.page';
import { ViewInvoicesPage } from '../view-invoices/view-invoices.page';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {
  
  products: any[] =[];
  categoreis$;

  public items1 = [];
  public itemsBackup=[]; 
  public type1 = [];
  searchQuery: string = ''; 
  filter:string;


  //1 define arrays 
  private items2;
  private item : Item = {} as Item;
  countItems=0; 
  constructor(private actionSheetCtrl: ActionSheetController, public alertController:AlertController, public afs:AngularFirestore,  public alertCtrl:AlertController, public activatedRoute: ActivatedRoute, private modalCtrl:ModalController , private data: DataService) { }

  ngOnInit() {
    //this.items1 = this.data.getItems();
    this.type1 = this.data.type2;

          //2 retrive items
          this.data.getItems().subscribe(
            res=>{
              this.items2=res; 
              this.itemsBackup=res; 
              this.itemsBackup.forEach((item)=>{
                this.countItems++; 
                this.afs.collection('User').doc(item.SubID.toString()).valueChanges().subscribe((user:any)=>{
                  console.log(user); 
                  item.Supplier=user.firstname;});
              })
            }
           );
  }

  filterItems(){
    console.log(this.itemsBackup);
    this.items2= this.itemsBackup.filter((item)=>{
      return this.filter==null||(this.filter=='category'&& item.type1.includes(this.searchQuery))
      ||(this.filter=='price'&& item.price==this.searchQuery)
      ||(this.filter=='quantity'&& item.quantity==this.searchQuery)
      ||(this.filter=='moving'&& item.moving==this.searchQuery)
      ||(this.filter=='supplier'&& item.Supplier.includes(this.searchQuery))
    })
  }

  async selectFilter(){
    if(this.filter=='price'){
      const actionSheet = await this.actionSheetCtrl.create({
        header: 'price sort',
        buttons: [
          {
            text: 'price-High to low',
            role: 'destructive',
            data: 'desc' 
            
          },
          {
            text: 'price-Low to High',
            role: 'destructive',
            data: 'asc'
          },
          {
            text: 'Cancel',
            role: 'cancel',
            data: {
              action: 'cancel',
            },
          },  
        ],
      });
  
      await actionSheet.present();
  
      const result = await (await actionSheet.onDidDismiss()).data as string;
      console.log(result);
      this.items2.sort((item1, item2) => {
        console.log('sort :>> ', item1, item2);
        if(result=='desc'){
          return item2.price - item1.price;
        } else {
          return item1.price - item2.price;
        }
      });
      console.log(this.items2);

      
    }

    if(this.filter=='quantity'){
      const actionSheet = await this.actionSheetCtrl.create({
        header: 'quantity sort',

        buttons: [
          {
            text: 'quntity-High to low',
            role: 'destructive',
            data: 'desc' 
            
          },
          {
            text: 'quntity-Low to High',
            role: 'destructive',
            data: 'asc'
          },
          {
            text: 'Cancel',
            role: 'cancel',
            data: {
              action: 'cancel',
            },
          },  
        ],
      });
  
      await actionSheet.present();
  
      const result = await (await actionSheet.onDidDismiss()).data as string;
      console.log(result);
      this.items2.sort((item1, item2) => {
        console.log('sort :>> ', item1, item2);
        if(result=='desc'){
          return item2.quantity - item1.quantity;
        } else {
          return item1.quantity - item2.quantity;
        }
      });
      console.log(this.items2);

      

      
    }
  }

  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }


  async viewInvoices()
  {

    const modal = await this.modalCtrl.create({
      component: ViewInvoicesPage,
      animated: true,
      mode: 'ios',
      backdropDismiss: true,

  
    })
    return await modal.present();
  }



  async historyOrders()
  {
    const modal = await this.modalCtrl.create({
      component: HistoryOrdersPage,
      animated: true,
      mode: 'ios',
      backdropDismiss: true,
  
    })
    return await modal.present();
  }

  async filter1(ev:any){
    this.items2=this.itemsBackup;
    
    let val = ev.target.value;
  
    if (val && val.trim() != '') {
      this.items2 = this.items2.filter((item) => {
      return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }


    console.log(ev);}
}
