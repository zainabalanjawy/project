import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OrderDetailsPage } from '../order-details/order-details.page';
@Component({
  selector: 'app-supplier-home-page',
  templateUrl: './supplier-home-page.page.html',
  styleUrls: ['./supplier-home-page.page.scss'],
})
export class SupplierHomePagePage implements OnInit 
{
  list=[
        {id:"cD4144",user:'Abas',is_fav:false,status:'Submited'},
        {id:"XGE3742",user:'Omer',is_fav:true,status:'Delivered'},
        {id:"cD2341",user:'Adnan',is_fav:false,status:'Completed'},
        {id:"cwer9745",user:'Ali',is_fav:false,status:''},
        {id:"cD2341",user:'Aziz',is_fav:false,status:''},
      ];


  constructor(public modalCtrl:ModalController) { }

  ngOnInit() {
  }


  async goOrderDetails()
  {
    const modal = await this.modalCtrl.create({
      component: OrderDetailsPage,
      animated: true,
      mode: 'ios',
      backdropDismiss: false,
  
    })
    return await modal.present();
  }
}