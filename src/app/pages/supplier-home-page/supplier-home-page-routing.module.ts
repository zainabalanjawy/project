import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SupplierHomePagePage } from './supplier-home-page.page';

const routes: Routes = [
  {
    path: '',
    component: SupplierHomePagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SupplierHomePagePageRoutingModule {}
