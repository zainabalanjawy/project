import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SupplierHomePagePageRoutingModule } from './supplier-home-page-routing.module';

import { SupplierHomePagePage } from './supplier-home-page.page';

@NgModule
({
  declarations: [SupplierHomePagePage],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SupplierHomePagePageRoutingModule
  ],
  schemas: 
  [  CUSTOM_ELEMENTS_SCHEMA  ]
})
export class SupplierHomePagePageModule {}
