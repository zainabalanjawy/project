import { Injectable } from '@angular/core';
import firebase from 'firebase/compat/app'
import 'firebase/compat/database' 
import { AngularFirestore} from '@angular/fire/compat/firestore';
import { AngularFirestoreCollection } from '@angular/fire/compat/firestore';
import { DocumentReference } from '@angular/fire/compat/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Timestamp } from 'rxjs-compat';
import { AuthService } from './services/auth.service';
import { User } from 'firebase/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';


export interface Types {
  type:string,
  image: string
}

export interface cart{
  id?:string,
  items:Item[],
  Total:number,
  uid:string, 
  Is_placed:boolean
}



export interface order{
  id?:string,
  is_fav:boolean, 
  status:string, 
  items1:Item[],
  total:number, 
  lastAdded:Item
}

//1 create item interface 
export interface Item {
  id?: string,
  type1:string,
  name: string,
  price: number,
  image: string,
  quantity: number, 
  thershold: number,
  orderQTY: number, 
  pastAdded:number, 
  Date:Date, 
  SubID:String, 
  ipCartoon:Number, 
  Cartoon:Number
}


// create schedule interface
export interface Schedule 
{
  id? : string,
  Date: Date,
  Shift1: string,
  Shift2: string
}

@Injectable({
  providedIn: 'root'
})

export class DataService {
  
  //2 create observables 
  private item: Observable<Item[]>;
  private itemCollectionRef: AngularFirestoreCollection<Item>;
 
  private Cart: Observable<cart[]>;
  private CartCollectionRef: AngularFirestoreCollection<cart>;

  
  private Order: Observable<order[]>;
  private OrderCollectionRef: AngularFirestoreCollection<order>;
  
  //3 initilize arrays 
  public Itemarray:Item[]=[]; 
  public useritem:Item[]=[];
  public categories:any[]=[];


  public countfruit=0; 
  public countchips=0; 
  public countdiary=0; 
  public total=0; 


   itemsToOrder = [];
   

  constructor(public afd:AngularFireDatabase, public afs:AngularFirestore, public afa:AngularFireAuth, public auth:AuthService) { 
    //4 get item document 
    this.itemCollectionRef = this.afs.collection<Item>('items');
    this.CartCollectionRef = this.afs.collection('Cart');
    this.Cart = this.CartCollectionRef.valueChanges({idField:'id'});
    
    this.OrderCollectionRef = this.afs.collection('User').doc(this.auth.uid).collection('Order');
    this.Order = this.OrderCollectionRef.valueChanges({idField:'id'});
  
    //5 read items from database 
    this.item = this.itemCollectionRef.snapshotChanges().pipe(     
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );



        //5 read orders from database 
        this.Order = this.OrderCollectionRef.snapshotChanges().pipe(     
          map(actions => {
            return actions.map(a => {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            });
          })
        );


      

  }

  //6 read categories from database 
  /*async readCategories(): Promise<any> {
    await firebase.database().ref('items').once('value', (snapshot) => {
    this.categories = snapshot.val();
    console.log(this.categories);
    });
    return this.categories;
    }
    */ 

    //7 get categories 
   /* getAllCategories(){
      return this.categories;
    }
    */


      
    getuser()
    {
      return this.afs.collection('User').valueChanges();
    }
  
    getschedule()
    {
      return this.afs.collection('Schedule').valueChanges();
    }
  
    
    //8 get items 
    getItems():Observable<Item[]> 
    {
      return this.item;
    }
      

        //8 get orders 
        getOrders():Observable<order[]> 
        {
          return this.Order;
        }

    //9 get item 
    getItem(id: string): Observable<Item> {
      return this.itemCollectionRef.doc<Item>(id).valueChanges().pipe(
     map(item2 => {
      item2.id = id;
      return item2
      })
     );
      }


      //add item 
      /*addItem1(item:Item): Promise<DocumentReference> {
              return this.itemCollectionRef.add(item); 
          }
        */
        

    //8 add item 
    /*addItem(item):Promise<any>{
      if(item.category == 'Fruits & Vegetables') {this.countfruit++;}
  
     else if(item.category == 'Chips & Snacks') {this.countchips++;}
  
     else if(item.category == 'Diary & Beverages') {this.countdiary++;}
     return this.itemCollectionRef.add(item);
  
    }
    */ 




    /*createProduct(name: string, description: string, quantity: number) {

      this.itemCollectionRef.add({ 
        type1:'Diary & Beverages',
        name: 'Milk',
        price: 4.2,
        image: '../../assets/d1.jpg',
        quantity: 2, 
        thershold: 3, 
        orderQTY: 2
       });
      }
*/ 
    //9 update item 
  /*  updateItem(upitem:Item){
      return this.itemCollectionRef.doc(upitem.id).update(upitem);
       }
    */


  

   public type2: Types[]=[
      {
        type:'Fruits & Vegetables',
        image: '../../assets/f&g.jpg'
      }, 
      {
        type:'Chips & Snacks',
        image: '../../assets/c&s.jpg'
      }, 
      {
        type:'Diary & Beverages',
        image: '../../assets/d&b.jpg'
      }
     ];
     

     updatestatus(arr)
     {
       console.log(arr)
     }
   

     item3: Item={} as Item;
     getItemId(index):Item
     {
      this.item.subscribe((res) => {
      this.item3.id = res[index].id;
      this.item3.name = res[index].name;
      this.item3.price= res[index].price;
      this.item3.quantity= res[index].quantity;
  
         /*  for(var i=1;i<=Number(this.item3.quantity);i++)
                 { this.qtyarr.push(i); }
                 */ 
      this.item3.image= res[index].image;
      this.item3.thershold= res[index].thershold;
      this.item3.orderQTY= res[index].orderQTY;
      this.item3.type1= res[index].type1;
      this.item3.pastAdded= res[index].pastAdded;
      this.item3.Date= res[index].Date;
      this.item3.SubID= res[index].SubID;
      this.item3.ipCartoon= res[index].ipCartoon;
      this.item3.Cartoon= res[index].Cartoon;
      return this.item3;
    })
    return this.item3;
  }

  cart1:cart= {} as cart; 
  addCart(item,uid)
  {
    
    //console.log(item);
    //console.log(uid);
    return this.afs.collection('User').doc(uid).collection('Cart').doc(item.id).set(item);
  }


  placeOrder(userOrder)
  {
    userOrder.total=this.total; 
    return this.afs.collection('User').doc(this.auth.uid).collection('Order').add(userOrder); 
  }



  /*createOrder(userOrder)
  {
    console.log(userOrder.id)
    this.afs.collection('User').doc(this.auth.uid).collection('Order').doc(userOrder.id).set(
      {
        is_fav:userOrder.is_fav, 
        status:userOrder.status, 
        items1:userOrder.items1,
        total:userOrder.total, 
        lastAdded:userOrder.lastAdded
      }
    )

  }

  updateOrder(userOrder)
  {
    
   // let userOrder1;
  //  userOrder1=this.afs.collection('User').doc(this.auth.uid).collection('Order').doc(userOrder.SubID).get()

     
 // console.log('userOrder1',userOrder1)
 // userOrder.total+= userOrder1.total;
  ///console.log('userOrder.total',userOrder.total)
  //userOrder1.item1.push(userOrder.lastAdded);

  console.log('userOrder',userOrder)
  this.afs.collection('User').doc(this.auth.uid).collection('Order').doc(userOrder.id).update({
      items1:userOrder.items1,
      total:userOrder.total, 
    });
    
  }*/

  deleteCart()
  {
    this.afs.collection('User').doc(this.auth.uid).collection('Cart').valueChanges({idField:'id'}).forEach(
      res=>{
      for(let i=0;i<res.length;i++)
      {
        this.afs.collection('User').doc(this.auth.uid).collection('Cart').doc(res[i].id).delete();
      }
    })
  }
    
  getOrder(id:string): Observable<order> {
    return this.OrderCollectionRef.doc<order>(id).valueChanges().pipe(
       map(order => {
      order.id = id;
      return order
      })
     );
   }
   
   temp_order:order={}as order;
   getOrderbyId(id:any):order{
     this.Order.subscribe((res) => {
       this.temp_order = res[id];  
       return this.temp_order;
     })
     return this.temp_order;
   }
   
/*
  getItem() {

    let item1: Item = {
      id: '1',
      type1:this.type2[0],
      name: 'Strawberry',
      price: 2.1,
      image: '../../assets/f1.jpg', 
      quantity: 11, 
      thershold: 10, 
      orderQTY:0
    }

    let item2: Item = {
      id: '2',
      type1:this.type2[0],
      name: 'Apple',
      price: 0.4,
      image: '../../assets/f2.jpg',
      quantity: 15, 
      thershold: 10, 
      orderQTY:0
    }

    let item3: Item = {
      id: '3',
      type1:this.type2[0],
      name: 'Mango',
      price: 0.6,
      image: '../../assets/f3.jpg', 
      quantity: 12, 
      thershold: 10, 
      orderQTY:0
    }

    let item4: Item = {
      id: '4',
      type1:this.type2[0],
      name: 'Orange',
      price: 0.5,
      image: '../../assets/f4.jpg', 
      quantity: 20, 
      thershold: 10, 
      orderQTY:0
    }

    let item5: Item = {
      id: '5',
      type1:this.type2[0],
      name: 'Blueberries',
      price: 1.5,
      image: '../../assets/f5.jpg', 
      quantity: 30, 
      thershold: 20, 
      orderQTY:0
    }

    let item6: Item = {
      id: '6',
      type1:this.type2[0],
      name: 'Tometos',
      price: 0.5,
      image: '../../assets/v1.jpg', 
      quantity: 15, 
      thershold: 20, 
      orderQTY:0
    }

    let item7: Item = {
      id: '7',
      type1:this.type2[0],
      name: 'Carrots',
      price: 0.7,
      image: '../../assets/v2.jpg', 
      quantity: 5, 
      thershold: 10, 
      orderQTY:0
    }

    let item8: Item = {
      id: '8',
      type1:this.type2[1],
      name: 'Mixed Crakers',
      price: 1.3,
      image: '../../assets/s1.jpg', 
      quantity: 11, 
      thershold: 10, 
      orderQTY:0
    }

    let item9: Item = {
      id: '9',
      type1:this.type2[1],
      name: 'Lays barbecue',
      price: 0.6,
      image: '../../assets/s2.jpg', 
      quantity: 11, 
      thershold: 10, 
      orderQTY:0
    }

    let item10: Item = {
      id: '10',
      type1:this.type2[1],
      name: 'Suger Crakers',
      price: 0.9,
      image: '../../assets/s3.jpg', 
      quantity: 40, 
      thershold: 10, 
      orderQTY:0
    }

    let item11: Item = {
      id: '11',
      type1:this.type2[1],
      name: 'Oreo',
      price: 1.0,
      image: '../../assets/s4.jpg', 
      quantity: 18, 
      thershold: 12, 
      orderQTY:0
    }


    let item12: Item = {
      id: '12',
      type1:this.type2[2],
      name: 'Pepsi',
      price: 3.1,
      image: '../../assets/b1.jpg', 
      quantity: 20, 
      thershold: 80, 
      orderQTY:0
    }

    
    let item13: Item = {
      id: '13',
      type1:this.type2[2],
      name: 'Merai Milk',
      price: 1.9,
      image: '../../assets/b2.jpg', 
      quantity: 12, 
      thershold: 33, 
      orderQTY:0
    }


    let item14: Item = {
      id: '14',
      type1:this.type2[2],
      name: 'Cocacola',
      price: 2.5,
      image: '../../assets/b3.jpg', 
      quantity: 17, 
      thershold: 90, 
      orderQTY:0
    }
    

    let item15: Item = {
      id: '15',
      type1:this.type2[2],
      name: 'Mixed fruites juice',
      price: 2.3,
      image: '../../assets/b4.jpg', 
      quantity: 60, 
      thershold: 12, 
      orderQTY:0
    }
    

    let item16: Item = {
      id: '16',
      type1:this.type2[2],
      name: 'Activea yougurt',
      price: 1.2,
      image: '../../assets/b5.jpg', 
      quantity: 44, 
      thershold: 12, 
      orderQTY:0
    }
    

   
  
    this.Itemarray.push(item1,item2,item3,item4,item5,item6,item7,item8,item9,item10,item11,item12,item13,item14,item15,item16);

    return this.Itemarray;
  }

  */ 



  /*
  getItemsToOrder()
  {
    let item1: Item = {
      id: '1',
      type1:this.type2[0],
      name: 'Strawberry',
      price: 2.1,
      image: '../../assets/f1.jpg', 
      quantity: 11, 
      thershold: 10,
      orderQTY:2
    
    };
    let item2: Item = {
      id: '15',
      type1:this.type2[2],
      name: 'Mixed fruites juice',
      price: 2.3,
      image: '../../assets/b4.jpg', 
      quantity: 60, 
      thershold: 12, 
      orderQTY:4
    };
    
    this.itemsToOrder.push(item1,item2);

    return this.itemsToOrder;
  }
  */
}
