import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes  } from '@angular/router';
import { redirectLoggedInTo,redirectUnauthorizedTo} from '@angular/fire/auth-guard';

const redirectUnauthorizedToLogin= () =>redirectUnauthorizedTo(['']);
const redirectLoggedInToHome= () =>redirectLoggedInTo(['home']);


const routes: Routes = 
[
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule), 
   // ...canActivate(redirectLoggedInToHome)
  
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./pages/welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'items',
    loadChildren: () => import('./pages/items/items.module').then( m => m.ItemsPageModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./pages/details/details.module').then( m => m.DetailsPageModule)
  },
  {
    path: 'items-details/:i',
    loadChildren: () => import('./pages/items-details/items-details.module').then( m => m.ItemsDetailsPageModule)
  },

  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'scheduling',
    loadChildren: () => import('./pages/scheduling/scheduling.module').then( m => m.SchedulingPageModule)
  },
  {
    path: 'viewshift',
    loadChildren: () => import('./pages/viewshift/viewshift.module').then( m => m.ViewshiftPageModule)
  },
  {
    path: 'tradereq',
    loadChildren: () => import('./pages/tradereq/tradereq.module').then( m => m.TradereqPageModule)
  },
  {
    path: 'assignshift',
    loadChildren: () => import('./pages/assignshift/assignshift.module').then( m => m.AssignshiftPageModule)
  },
  {
    path: 'favorite-orders-list',
    loadChildren: () => import('./pages/favorite-orders-list/favorite-orders-list.module').then( m => m.FavoriteOrdersListPageModule)
  },
  {
    path: 'past-orders',
    loadChildren: () => import('./pages/past-orders/past-orders.module').then( m => m.PastOrdersPageModule)
  },
  {
    path: 'view-order',
    loadChildren: () => import('./pages/view-order/view-order.module').then( m => m.ViewOrderPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
  },   
  {
    path: 'cart',
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
  }, 
  {
    path: 'reports',
    loadChildren: () => import('./pages/reports/reports.module').then( m => m.ReportsPageModule)
  },
  {
    path: 'view-invoices',
    loadChildren: () => import('./pages/view-invoices/view-invoices.module').then( m => m.ViewInvoicesPageModule)
  },
  {
    path: 'history-orders',
    loadChildren: () => import('./pages/history-orders/history-orders.module').then( m => m.HistoryOrdersPageModule)
  },
  {
    path: 'supplier-home-page',
    loadChildren: () => import('./pages/supplier-home-page/supplier-home-page.module').then( m => m.SupplierHomePagePageModule)
  },
  {
    path: 'empsch/:id',
    loadChildren: () => import('./pages/empsch/empsch.module').then( m => m.EmpschPageModule)
  },
 
  
  {
    path: 'homemp',
    loadChildren: () => import('./pages/homemp/homemp.module').then( m => m.HomempPageModule)
  },  
  {
    path: 'trade-req/:id',
    loadChildren: () => import('./pages/trade-req/trade-req.module').then( m => m.TradeReqPageModule)
  },
  {
    path: 'req',
    loadChildren: () => import('./req/req.module').then( m => m.ReqPageModule)
  },
  {
    path: 'trade',
    loadChildren: () => import('./trade/trade.module').then( m => m.TradePageModule)
  } ,
  {
    path: 'order-details',
    loadChildren: () => import('./pages/order-details/order-details.module').then( m => m.OrderDetailsPageModule)
  },
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
function canActivate(): import("@angular/router").Route {
  throw new Error('Function not implemented.');
}

