import { Component } from '@angular/core';
import { Route, Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent
{
  role;

  constructor(public auth:AuthService, public router:Router)
  {
    
  

    if( this.auth.isLoggedIn==true )   
    {
      
      
      //this.router.navigate(['/items'])
    }

   /* if( this.auth.isLoggedIn==true)
    {
      this.router.navigateByUrl('/items')
    }*/
   
    else if ( this.auth.isLoggedIn==false)
    {
      this.router.navigate(['/welcome'])
    }
    
  
  }



  userrole ="owner";
  
  pages = [
    {
      title: 'Home',
      url: '/items',
      icon: 'home'
    },
    {
      title: 'Scheduling',
      url: '/scheduling',
      icon: 'time'
    },
    {
      title: 'Rports',
      url: '/reports',
      icon: 'bar-chart'
    },
    {
      title: 'Acount',
      url: '/account',
      icon: 'person'
    },
    {
      title: 'sign-out',
      url: '/account',
      icon: 'log-out'
    }
  ];


  


  
}



