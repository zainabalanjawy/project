import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { DataService } from '../data.service';
import { AuthService,User } from 'src/app/services/auth.service';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { addDoc, collection, getDocs, getFirestore, query, where } from 'firebase/firestore';
import { RouterLink } from '@angular/router';

export interface Profile
{
username:string; 
role:string; 
}

@Component({
  selector: 'app-trade',
  templateUrl: './trade.page.html',
  styleUrls: ['./trade.page.scss'],
})

 
export class TradePage implements OnInit {
  
  users$;
  schedual$;
  uid;
  datee;


  constructor(public afs:AngularFirestore, public authService:AuthService, public Dataservs:DataService,public popoverController:PopoverController,) 
  {

    this.uid = localStorage.getItem("uid")

    this.users$ = this.Dataservs.getuser();
    this.schedual$ = this.Dataservs.getschedule();

  }

  firtsAprv()
  {

  }


  async trade()
  {
    let partYouWant;
    const dateArray = this.datee.split('T');
    if (dateArray.length > 0)
    {
      partYouWant = dateArray[0];
    }


    const db = getFirestore()
    const colRef = collection(db, 'Schedule')
    const q = query(colRef, where("Date", "==", partYouWant));

    const shifts = await getDocs(q);
    let arr=[];

    shifts.docs.map((d) => 
    {

      arr.push({...d.data(), id: d.id})

    });

    console.log(arr);

    
    if(arr[0].Shift1 == this.uid)
    {
      this.afs.collection('Schedule').doc(arr[0].id).update
      ({
        To:arr[0].Shift2,
                
      });
    }
  

    if(arr[0].Shift2 == this.uid)
    {
      this.afs.collection('Schedule').doc(arr[0].id).update
      ({
        To:arr[0].Shift1,
   
      });
    }
   
  }

  ngOnInit() {
  }

  pages =
  [
    {
      title: 'Acount',
      url: '/account',
      icon: 'person'
    },
    {
      title: 'sign-out',
      url: '/account',
      icon: 'log-out'
    },
    {
      title: 'Home',
      url: '/homemp',
      icon: 'home'
    }
  ];

  async DismissClick() 
  {
    await this.popoverController.dismiss();
  }
}
